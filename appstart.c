/* AUTHOR 				: 		Alexander Dausel
/* E-MAIL				:		aldait03@hs-esslingen.de
/* MATRIKELNUMMER       : 		757838
/* VERSION				:		1.0		(gefuehlt 10.0)
/* IDE					:		ATOM (Auf Linux Mint)
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>

/****** LOS GEHT'S ******/

typedef struct{

	char processName[10];
	pid_t childProcessID; // structs sind 'ne tolle Sache
	pid_t parentProcessID;
	bool activeClient;

}Processes;

Processes client[3]; // struct-Array fuer je einen Client

Processes *ptrClient0 = &client[0];
Processes *ptrClient1 = &client[1]; // Pointer auf struct Arrays sind 'ne tolle Sache
Processes *ptrClient2 = &client[2];

/****** Bekanntmachung der Funktionen ******/
void xterm();
void xclock();
void xload();
void menue();
void appstart();
void killMenue();
void appstart();
/*******************************************/

/****** Globale Variablen (einfachheitshalber) ******/
int wahl = 0;
pid_t processCloneXterm = 0;
pid_t processCloneXclock = 0;
pid_t processCloneXload = 0;
int processCounter = 0;
/****************************************************/

/****** fflush(stdin) fuer echte Maenner ******/
void clean_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}
/**********************************************/

void xterm(){
	processCloneXterm = fork();
	if (processCloneXterm == 0){
		/****** IM CHILD PROCESS ******/
		ptrClient0->childProcessID = getpid();
		printf("Client xterm mit der PID %i gestartet\n",ptrClient0->childProcessID);
		execlp( "xterm", "xterm", NULL);
		/******************************/
	}
	else {
		/****** IM PARENT PROCESS ******/
		strcpy(ptrClient0->processName,"xload");
		ptrClient0->parentProcessID = processCloneXterm; // PID des Childs!
		ptrClient0->activeClient = true;
		++processCounter;

	}


}

void xclock(){
	processCloneXclock = fork();

	if (processCloneXclock == 0){
		ptrClient1->childProcessID = getpid();
		printf("Client xclock mit der PID %i gestartet\n",ptrClient1->childProcessID);
		execlp( "xclock", "xclock", NULL);

	}
	else {

		strcpy(ptrClient1->processName,"xclock");
		ptrClient1->parentProcessID = processCloneXclock;
		ptrClient1->activeClient = true;
		++processCounter;

	}

}

 void xload(){

	processCloneXload = fork();

	if (processCloneXload == 0){
		ptrClient2->childProcessID = getpid();
		printf("Client xload mit der PID %i gestartet\n",ptrClient2->childProcessID);
		execlp( "xload", "xload", NULL);

	}
	else {

		strcpy(ptrClient2->processName,"xload");
		ptrClient2->parentProcessID = processCloneXload;
		ptrClient2->activeClient = true;
		++processCounter;

	}

}

void endAllProcesses(){
	kill(ptrClient0->parentProcessID,SIGTERM);
	ptrClient0->parentProcessID = 0;
	ptrClient0->activeClient = false;

	kill(ptrClient1->parentProcessID,SIGTERM);
	ptrClient1->parentProcessID = 0;
	ptrClient1->activeClient = false;

	kill(ptrClient2->parentProcessID,SIGTERM);
	ptrClient2->parentProcessID = 0;
	ptrClient2->activeClient = false;

}

void menue(int processCounter){ // processCounter beim ersten Start 0!

	printf("\n\nProgramm zum Start auswaehlen: ");
	printf("\n\n\t[1] <<< xterm >>>\n");
	printf("\n\t[2] <<< xclock >>>\n");
	printf("\n\t[3] <<< xload >>>\n");

	if (processCounter != 0){printf("\n\t[9] <<< Client beenden >>>\n");}

	printf("\n\t[0] <<< Beenden >>>\n");
	printf("\n\nIhre Eingabe: ");
	scanf("%i",&wahl);
	clean_stdin();
}

void appstart(){

	switch(wahl){

		case 1 : xterm();break;
		case 2 : xclock();break;
		case 3 : xload();break;
		case 9 : killMenue();break;
		case 0 : endAllProcesses();break;
		default : printf("Ungueltige Eingabe!");break;

	}
}

void killMenue(){

	int killChoice = 0;

	printf("\n\nProgramm zum sterben auswaehlen: ");
	printf("\n\n\t[0] <<< kein Programm beenden >>>\n");

	if (ptrClient0->activeClient) {printf("\n\n\t[1]<<< xterm (%i) >>>",ptrClient0->parentProcessID);}

	if (ptrClient1->activeClient) {printf("\n\n\t[2]<<< xclock (%i) >>>",ptrClient1->parentProcessID);}

	if (ptrClient2->activeClient) {printf("\n\n\t[3]<<< xload (%i) >>>",ptrClient2->parentProcessID);}


	printf("\n\n");

	scanf("%i",&killChoice);

	clean_stdin();

	switch(killChoice){

		case 0 : break;
		case 1 : kill (ptrClient0->parentProcessID, SIGTERM);
						--processCounter;printf("Prozess %s mit der PID %i beendet!",ptrClient0->processName,ptrClient0->parentProcessID);strcpy(ptrClient0->processName,"\0");ptrClient0->activeClient = false;break;
		case 2 : kill (ptrClient1->parentProcessID, SIGTERM);
						--processCounter;printf("Prozess %s mit der PID %i beendet!",ptrClient1->processName,ptrClient1->parentProcessID);strcpy(ptrClient1->processName,"\0");ptrClient1->activeClient = false;break;
		case 3 : kill (ptrClient2->parentProcessID, SIGTERM);
						--processCounter;printf("Prozess %s mit der PID %i beendet!",ptrClient2->processName,ptrClient2->parentProcessID);strcpy(ptrClient2->processName,"\0");ptrClient2->activeClient = false;break;
	}
}

int main(){

	do {

		menue(processCounter);
		appstart();

	}while(wahl != 0);
}
